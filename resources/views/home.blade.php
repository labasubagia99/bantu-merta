@extends('layouts.app')

@section('content')
    <select id="select" onchange="getData(this.value)">
    </select>
    <div id="data"></div>
@endsection
@section('js')
<script>
    const getData = async (id=null) => {
        const res =  await fetch(`/data/home/${id || ''}`);
        const data = await res.json();

        let category = '<option value="">--Pilih--</option>';
        let product = '';

        for (let v of data.category) {
            category += `<option ${ v.id == id ? 'selected' : '' } value="${v.id}"> ${v.name}</option>`;
        }
        for (let v of data.product) {
            product += `<p>${v.name} <br> --${v.category} </p>`;
        }
        document.getElementById('select').innerHTML = category;
        document.getElementById('data').innerHTML = product;
    }
    getData();
</script>
@endsection 