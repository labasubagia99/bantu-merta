<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    public function index($id=null) {
        $data = Product::all();
        $category = Category::all();

        $tmpProduct = [];
        $product = [];
        foreach ($data as $v) {
            $tmpProduct[] = (object) [
                'id'            => $v->id,
                'name'          => $v->name,
                'category_id'   => $v->category->id,
                'category'      => $v->category->name,  
            ];
        }

        if ($id) {
            foreach ($tmpProduct as $v) {
                if ($v->category_id == $id) {
                    $product[] = $v;
                }
            }
        }
    
        return response()->json([
            'product'   => $id ? $product : $tmpProduct, 
            'category'  => $category,
        ]);
    }
}
